# loadconf

Config files make it easy for users to use your program the way they
want to. With loadconf you can easily give users that power.

## Install

The usual way:

`pip install loadconf`

Requires python3

# Usage

I think this module is best explained through example, so here you go!

## user = Config("my_program")

``` python
>>> from loadconf import Config
>>> user = Config("my_program")
>>> user._program
'my_program'
>>> user._platform
'linux' # or macos, or windows
```

To initialize the `Config` object you only need to give the name of your
program, or whatever name you like. As you can see there are some
reserved values after initialization.

## user.define_settings()

``` python
>>> settings = { "fav_color": "Orange", "fav_int": 1, "fav_bool": True }
>>> user.define_settings(settings)
>>> user.settings["fav_color"]
'Orange'
```

Users may not provide all settings that are relevant to your program. If
you want to set some defaults, this makes it easy.

## user.define_files()

``` python
>>> user_files = { "conf": "my_program.conf" }
>>> user.define_files(user_files)
>>> user.files["conf"]
'/home/user/.config/my_program/my_program.conf'     # on Linux
'/home/user/Library/Preferences/my_program.conf'    # on MacOS
'C:\\Users\\user\\AppData\\Local\\my_program.conf'  # on Windows
>>> user.files # on Linux
{'conf': '/home/user/.config/my_program/my_program.conf'}
```

Why you might use this:

- Finds where config files should get installed by default
- Gives a quick way to access a file by it's key
- Allows for access via keys when calling other methods like:
  - `create_files()`
  - `read_conf()`
  - `store_files()`
  - `associate_settings()`
  - `create_template()`

## user.associate_settings()

``` python
# same settings dict used for user.define_settings()
>>> settings = { "fav_color": "Orange", "fav_int": 1, "fav_bool": True }
>>> user.associate_settings(list(setting.keys()), "conf")
>>> user.settings_files
# Formatted for legibility
{
    'conf': {
        'fav_bool': True,
        'fav_color': 'Orange',
        'fav_int': 1
    }
}
```

This method is necessary if you plan on using the `create_template()`
method. The purpose is to associate some settings with a particular
file.

**Parameters**:

- settings: List
- file: Str

**settings** should be a list of keys set by `define_settings()`.
**file** should a key for a file set by `define_files()`.

## user.create_files()

``` python
>>> file_list = ["conf", "/path/to/file/to/create.txt"]
>>> user.create_files(file_list)
```

If you've run `user.define_files` then you can pass a key that is
relevant to `user.defined_files`. That will create the file value of
that key. If an item in the given list is not a key then it will get
created if it is an absolute file path.

## user.create_template()

``` python
# same settings dict used for user.define_settings()
>>> settings = { "fav_color": "Orange", "fav_int": 1, "fav_bool": True }
>>> user.create_template(list(settings), "conf")
```

The above fill the the `conf` file like so:

``` conf
fav_color = Orange
fav_int = 1
fav_bool = True
```

This method allows for an easy way to create a default user file with
predefined settings. This method will only create templates for files
created by `create_files()`. The `create_files()` method only creates
files not found when running the program.

## user.read_conf()

Let's assume the config file we are reading looks like this:

``` conf
# my_program.conf
setting_name = setting value
fav_color = Blue
int_val = 10
bool_val = true
good_line = My value with escaped delimiter \= good time
```

To read the file we run this:

``` python
>>> settings = ["fav_color", "good_line", "int_val", "bool_val"]
>>> files = ["conf"]
>>> user.read_conf(settings, files)
>>> user.settings["fav_color"]
'Blue'
>>> user.settings["good_line"]
'My value with escaped delimiter = good time'
>>> user.settings["int_val"]
10
>>> user.settings["bool_val"]
True
```

Things to note:

- `read_conf()` will make effort to determine int and bool values for
  settings instead of storing everything as a string.
- If the user has a value that has an unescaped delimiter then
  `csv.Error` will get raised with a note about the line number that
  caused the error.
- The default delimiter is the equal sign `=` but you can set something
  different
- The default comment character is pound `#` but you can set it to
  something different
- For users to escape the delimiter character they can use a backslash.
  That backslash will not get included in the stored value.

## user.store_files()

``` python
>>> user.store_files({"other": "/path/to/unknown_file.txt"})
>>> user.stored["other"]
['line1', 'line2 with some text', 'line3', 'etc.']
>>> user.store_files(["conf"])
>>> user.stored["conf"]
['conf_line1', 'conf_line2 with some text', 'conf_line3', 'etc.']
```

The purpose of this method is to allow you to store each line of a file
in a list accessible through `user.stored["key"]`. Why might you want
this? Instead of forcing a brittle syntax on the user you can give them
an entire file to work with. If a variable is useful as a list then this
gives users an easy way to define that list.

If you've run `user.define_files()` then you can give
`user.store_files()` a list of keys that correspond to a defined file.
If you haven't defined any files then you can give a dict of files to
store and a key to store them under.

Storing json data can be nice too though:

``` python
>>> user.store_files({"json_file": "/path/to/data.json"}, json_file=True)
>>> user.stored["json_file"]
{'my_json_info': True}
```
